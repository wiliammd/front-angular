import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ModelResponseDTO } from 'src/app/models/response.dto';
import { NetworkUtil } from 'src/app/Utils/network-util';
import { ParametroModel } from '../../models/parametro.model';
import { GetProvider } from '../get-provider';
import { CnabDTO } from '../../models/input-file/input-file.dto';
@Injectable({
  providedIn: 'root'
})
export class InputFileGetProvider extends GetProvider {
  constructor(
    private http: HttpClient,
    private _networkUtil: NetworkUtil
  ) {
    super(http, _networkUtil);
    this.uri = 'enviarArquivo/';
  }

  async getManyPaged(pagina: number, params?: ParametroModel[]): Promise<ModelResponseDTO<CnabDTO>> {
    return this.get<CnabDTO>({ pagina, params });
  }



  async getOne(urlParams: string): Promise<CnabDTO> {
    let resposta = await this.get<CnabDTO>({})
    return resposta.items[0];
  }


}
