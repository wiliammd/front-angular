import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NetworkUtil } from 'src/app/Utils/network-util';
import { PostArquivosProvider } from '../post-provider-arquivos';
@Injectable({
    providedIn: 'root'
})
export class InputFilePostProvider extends PostArquivosProvider {

    constructor(private _http: HttpClient,
        private _networkUtil: NetworkUtil) {
        super(_http, _networkUtil);
        this.uri = 'enviarArquivo/';
    }

    async post(concorrenteDto: any): Promise<String> {
        return await this._post<String>(concorrenteDto);
    }
}
