export class CnabDTO {
  id: string;
  transacao: string;
  dataOcorrencia: Date;
  valor: number;
  cpf: string;
  cartao: string;
  nomeRepresenteLoja: string;
  nomeLoja: string;
}
