export class GrupoDTO {
    id: string;
    codigo: number;
    descricao: string;
    ativo: boolean;
    dateCreate: Date;
    sessaoCreate: string;
    dateUpdate: Date;
    sessaoUpdate: string;
    ordem:number;
}