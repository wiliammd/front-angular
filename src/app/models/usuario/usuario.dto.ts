import { GrupoDTO } from '../grupo/grupo.dto';

export class UsuarioDTO {
    id: string = "";
    codigo: string = "";
    email: string = "";
    usuario: string = "";
    nome: string = "";
    grupoUsuario: GrupoDTO[] = new Array<GrupoDTO>();
    ativo: boolean = true;
    descricao: string  = "";
    dateCreate: Date;
    sessaoCreate: string = "";
    dateUpdate: Date;
    sessaoUpdate: string = "";
}
