import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PoModule } from '@po-ui/ng-components';
import { NetworkUtil } from './../../Utils/network-util';
import { InputFileInsertComponent } from './input-file-insert/input-file-insert.component';
import { InputFileListComponent } from './input-file-list/input-file-list.component';
import { InputFileRoutesRoutingModule } from './input-file-routing.module';


@NgModule({
  declarations:[InputFileInsertComponent,InputFileListComponent],
  providers: [NetworkUtil],
  imports: [
      CommonModule,
      InputFileRoutesRoutingModule,
      PoModule,
      FormsModule,
      ReactiveFormsModule,
  ]
})
export class InputFileModule { }
