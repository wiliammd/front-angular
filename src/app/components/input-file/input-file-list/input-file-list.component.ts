import { TipoTransacaoEnum } from './../../../models/input-file/TipoTransacaoEnum';
import { InputFileGetProvider } from './../../../services/input-file/input-file-get-provider';
import { NetworkUtil } from './../../../Utils/network-util';
import { Router } from '@angular/router';
import { PoDialogService, PoI18nService, PoNotificationService } from '@po-ui/ng-components';
import { Component, OnInit } from '@angular/core';
import { AbstractPage } from '../../abstract-page';
import { ParametroModel } from 'src/app/models/parametro.model';
import { CnabDTO } from 'src/app/models/input-file/input-file.dto';

@Component({
  selector: 'app-input-file-list',
  templateUrl: './input-file-list.component.html',
  styleUrls: ['./input-file-list.component.scss']
})
export class InputFileListComponent extends AbstractPage implements OnInit {
  cnabLista: Array<CnabDTO> = [];
  constructor(
    private poAlert: PoDialogService,
    private router: Router,
    public i18nService: PoI18nService,
    private networkUtil: NetworkUtil,
    private inputFileGetProvider: InputFileGetProvider,
    private notification: PoNotificationService,
  ) {
    super(i18nService);
  }

  ngOnInit(): void {
    this.title = this.literals.arquivosInseridos;
    this.hideLoading = true;
    this.breadcrumb = { items: [] };
    this.breadcrumb.items = this.breadcrumb.items.concat({ label: this.literals.home, link: '/' });
    this.breadcrumb.items = this.breadcrumb.items.concat({ label: this.literals.arquivosInseridos, link: undefined });
    this.colunas = [
      {
        property: 'transacao',
        type: 'label',
        label: this.literals.transacao,
        labels: [
          { value: TipoTransacaoEnum.DEBITO,color: 'color-10',  label: this.literals?.debito },
          { value: TipoTransacaoEnum.BOLETO,color: 'color-07', label: this.literals?.boleto },
          { value: TipoTransacaoEnum.FINANCIMENTO,color: 'color-07',  label: this.literals?.financiamento },
          { value: TipoTransacaoEnum.CREDITO,color: 'color-10',  label: this.literals?.credito },
          { value: TipoTransacaoEnum.RECEBIMENTOEMPRESTIMO,color: 'color-10',  label: this.literals?.recebimentoEmprestimo },
          { value: TipoTransacaoEnum.VENDAS, color: 'color-10', label: this.literals?.vendas },
          { value: TipoTransacaoEnum.RECEBIMENTOTED, color: 'color-10', label: this.literals?.recebimentoTed },
          { value: TipoTransacaoEnum.RECEBIMENTODOC, color: 'color-10', label: this.literals?.recebimentoDoc },
          { value: TipoTransacaoEnum.ALUGUEL,color: 'color-07',  label: this.literals?.aluguel }
        ]
      },
      { property: 'valor', label: this.literals.valor, type: 'currency', format: 'BRL', },
      { property: 'dataOcorrencia', label: this.literals.dataOcorrencia, type: 'dateTime' },
      { property: 'nomeRepresenteLoja', label: this.literals.nomeRepresenteLoja },
      { property: 'cpf', label: this.literals.cpf },
      { property: 'cartao', label: this.literals.cartao },
      { property: 'nomeLoja', label: this.literals.nomeLoja },
    ];

    this.createItens();
  }
  async showMore() {
    if (this.labelFilter) {
      this.parametros.push(new ParametroModel('filter', this.labelFilter));
    }
    await this.createItens(true, null, this.parametros);
  }

  async createItens(showMore?: boolean, pesquisa?: string, params?: ParametroModel[]) {
    try {
      this.hideLoading = false;

      this.prepareParameters(showMore, pesquisa, params);
      if (!showMore) {
        this.cnabLista = [];
      }

      let resposta = await this.inputFileGetProvider.getManyPaged(this.page, this.parametros);
      console.log(resposta);
      this.cnabLista = this.cnabLista.concat(resposta.items);

      if (this.cnabLista && resposta.items.length > 0 && resposta.hasNext) {
        this.carregaMais = true;
      } else {
        this.carregaMais = false;
      }

      this.hideLoading = true;

    } catch (erro) {
      this.networkUtil.exibeMsgErroRequisicao(erro);
      this.hideLoading = true;
    }
  }

  getListItems() {
    return this.cnabLista;
  }

}
