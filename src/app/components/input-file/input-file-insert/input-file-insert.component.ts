import { InputFilePostProvider } from '../../../services/input-file/input-file-post-provider';
import { NetworkUtil } from '../../../Utils/network-util';
import { Router } from '@angular/router';
import { PoI18nService, PoDialogService, PoNotificationService } from '@po-ui/ng-components';
import { Component, OnInit } from '@angular/core';
import { AbstractPage } from '../../abstract-page';
import { ParametroModel } from 'src/app/models/parametro.model';

@Component({
  selector: 'app-input-file-insert',
  templateUrl: './input-file-insert.component.html',
  styleUrls: ['./input-file-insert.component.scss']
})
export class InputFileInsertComponent extends AbstractPage implements OnInit {
  hideLoading: boolean = true;
  arquivo: File;
  formFile : any;
  arquivoPreview: any;
  constructor(
    private poAlert: PoDialogService,
    private router: Router,
    public i18nService: PoI18nService,
    private inputFilePostProvider:InputFilePostProvider,
    private networkUtil: NetworkUtil,
    private notification: PoNotificationService,
  ) {
    super(i18nService);
  }

  ngOnInit(): void {
    this.title = this.literals.enviarArquivo;
    this.hideLoading = true;
    this.breadcrumb = { items: [] };
        this.breadcrumb.items = this.breadcrumb.items.concat({ label: this.literals.home, link: '/' });
        this.breadcrumb.items = this.breadcrumb.items.concat({ label: this.literals.enviarArquivo, link: undefined });
  }
  async preview(event: Event) {
    const arquivos = (event.target as HTMLInputElement).files;
    if(arquivos[0].type == "text/plain"){
      this.arquivo = arquivos[0];
      const formData = new FormData();
      formData.append("file",this.arquivo,this.arquivo.name);
      this.formFile = formData;
    }else{
      (event.target as HTMLInputElement).files = null;
      this.notification.error("formato de arquivo selecionado invalido!")
    }
  }

  async enviarArquivo(){
    console.log(this.formFile);
    console.log(this.arquivo);
    if(this.formFile !=null){
    try {
     let response = await this.inputFilePostProvider.post(this.formFile);
     console.log(response);
      this.notification.success("arquivo enviado com sucesso!")
    } catch (error) {
      this.notification.error(error);
    }
  }else{
    this.notification.error("arquivo vazio");

  }
  }

  createItens(showMore?: boolean, pesquisa?: string, params?: ParametroModel[]) {
    throw new Error('Method not implemented.');
  }
}
