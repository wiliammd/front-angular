import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { InputFileInsertComponent } from './input-file-insert/input-file-insert.component';
import { InputFileListComponent } from './input-file-list/input-file-list.component';

export const inputFileRoutes: Routes = [
  {
    path: 'inserirArquivo',
    component: InputFileInsertComponent
  },
  {
    path: 'listar',
    component: InputFileListComponent
  },
];

@NgModule({
  imports: [RouterModule.forChild(inputFileRoutes)],
  exports: [RouterModule]
})
export class InputFileRoutesRoutingModule { }
